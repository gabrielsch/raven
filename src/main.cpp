#include <iostream>
#include "raven/application.h"
#include "raven/service.h"

int main() {

    Raven::Dummy dummy = Raven::Dummy();
    printf("Dummy address is <%p>\n", &dummy);

    Raven::ServiceLocator locator = Raven::ServiceLocator();
    locator.add(&dummy);

    Raven::Dummy* another = locator.get<Raven::Dummy>();
    printf("Dummy from service locator address is <%p>\n", another);

    Raven::ApplicationDefault app = ApplicationDefault(locator);
    app.run();

    //std::cout << "Hello, World!" << std::endl;
    return 0;
}