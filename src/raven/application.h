//
// Created by Gabriel Schneider on 9/25/2017.
//

#ifndef RAVEN_APPLICATION_H
#define RAVEN_APPLICATION_H

#include "service.h"
namespace Raven {
    class ApplicationInterface {
    protected:
        ServiceLocator serviceLocator;

    public:
        ApplicationInterface(ServiceLocator &serviceLocator);

        ServiceLocator getServiceLocator();

        void setServiceLocator(ServiceLocator serviceLocator);

        virtual bool run() = 0;
    };

    class ApplicationDefault : public ApplicationInterface {
    public:
        ApplicationDefault(ServiceLocator &serviceLocator);

        bool run() override;
    };
}

#endif //RAVEN_APPLICATION_H
