//
// Created by Gabriel Schneider on 9/25/2017.
//

#include "service.h"

namespace Raven {
    ServiceLocator *Service::getServiceLocator() const {
        return serviceLocator;
    }

    void Service::setServiceLocator(ServiceLocator *serviceLocator) {
        Service::serviceLocator = serviceLocator;
    }
}

