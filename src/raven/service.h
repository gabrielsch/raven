//
// Created by Gabriel Schneider on 9/25/2017.
//

#ifndef RAVEN_SERVICE_H
#define RAVEN_SERVICE_H

#include <iostream>
#include <vector>
#include <typeinfo>

namespace Raven {

    class Service {
        ServiceLocator* serviceLocator;
    public:
        ServiceLocator *getServiceLocator() const;
        void setServiceLocator(ServiceLocator *serviceLocator);
    };

    class Dummy : public Service {
    public:
    };


    class ServiceLocator {

    private:
        std::vector<Service *> services;
    public:
        template<class T>
        void add(T *service) {
            services.insert(services.end(), service);
        };

        template<class T>
        T *get() {
            for (auto *i : services) {
                if (typeid(*i) == typeid(T)) {
                    return (T *) i;
                }
            }
        }
    };
}
#endif //RAVEN_SERVICE_H
