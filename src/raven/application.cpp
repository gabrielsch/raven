//
// Created by Gabriel Schneider on 9/25/2017.
//

#include <iostream>
#include "application.h"

namespace Raven {

    bool ApplicationDefault::run() {
        std::cout << "Running..." << std::endl;
    }

    ApplicationDefault::ApplicationDefault(ServiceLocator &serviceLocator) : ApplicationInterface(serviceLocator) {

    }

    ServiceLocator ApplicationInterface::getServiceLocator() {
        return this->serviceLocator;
    }

    void ApplicationInterface::setServiceLocator(ServiceLocator serviceLocator) {
        this->serviceLocator = serviceLocator;
    }

    ApplicationInterface::ApplicationInterface(ServiceLocator &serviceLocator) {
        setServiceLocator(serviceLocator);
    }
}